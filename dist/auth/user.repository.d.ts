import { Repository } from "typeorm";
import { User } from "./user.entity";
import { AuthCredentialDto } from "./dto/auth-credential.dto";
export declare class UserRepository extends Repository<User> {
    signUp(authCredentialDto: AuthCredentialDto): Promise<void>;
    validateUserPassword(authCredentialDto: AuthCredentialDto): Promise<string>;
    private hashPassword;
}
