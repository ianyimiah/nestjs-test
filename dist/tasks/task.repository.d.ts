import { Repository } from "typeorm";
import { Task } from "./task.entity";
import { CreateTaskDto } from "./dtos/creat-task.dto";
import { GetTaskFilterDto } from "./dtos/get-task-filter.dto";
import { User } from "src/auth/user.entity";
export declare class TaskRepository extends Repository<Task> {
    getTasks(filterDto: GetTaskFilterDto, user: User): Promise<Task[]>;
    createTask(createTaskDto: CreateTaskDto, user: User): Promise<Task>;
}
