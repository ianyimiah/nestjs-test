import { IsString, MinLength, MaxLength, Matches } from "class-validator";

export class AuthCredentialDto {

    @IsString()
    @MinLength(4)
    @MaxLength(20)
    username: string;

    @IsString()
    @MinLength(8)
    @Matches(
        /((?=.*\d)|(?=.*\W+))(?=.*[A-Z])(?=.*[a-z]).*$/,
        { message: 'Password too weak' }
    ) // validates that the password has at least one lowercase character, at least one uppercase character and at least one number or special character
    password: string;

}